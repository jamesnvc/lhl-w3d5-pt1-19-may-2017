//
//  ViewController.m
//  ImageTesting
//
//  Created by James Cash on 19-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pickImage:(id)sender {
    UIImagePickerController *ipc = [[UIImagePickerController alloc] init];

    // is the camera available?
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        // if so, use that
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        // no camera, just use photo library
        ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }

    // what types of media can we ask for?
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:ipc.sourceType];
    NSLog(@"Media types are %@", availableMediaTypes);
    ipc.mediaTypes = availableMediaTypes;
    // if we wanted, we could limit to just images or just videos
    //ipc.mediaTypes = @[ kUTTypeImage];

    // we need to make ourselves the delegate of the image picker so we can see the picked image & do something with it
    ipc.delegate = self;

    [self presentViewController:ipc animated:YES completion:^{
        NSLog(@"Picker is showing");
    }];
}

#pragma mark - UIImagePickerControllerDelegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSLog(@"Finished picking %@", info);
    self.imageView.image = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        NSLog(@"done");
    }];
}

@end
